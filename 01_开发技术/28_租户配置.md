## 租户参数

### 场景
此文档针对的是:租户在门户配置好应用参数,比如系统参数，数据字典，应用在开发过程中需要调用。

### 调用方法

配置应用id和租户id

```yml
dubbo:
  application:
    key-id: 629589307002191872  # 应用id
    tenant-id: 622470063085060096  # 用户id
```

接口方法

```java
/**
 * 租户list查询
 * @param restWrapper
 * @return
 */
List<Entity> tenantFindList(@NonNull RestWrapper restWrapper);

/**
 * 租户one查询
 * @param restWrapper
 * @return
 */
Entity tenantFindOne(@NonNull RestWrapper restWrapper);
```

调用示例

```java
// 引用参数服务
@Reference
private IManagerSettingsService managerSettingsService

// 调用系统参数
List<ManagerSettingsEntity> list = managerSettingsService.tenantFindList(RestWrapper.create().eq("configKey", "sys.portal.style")) ;
System.out.println("list : " + JSONObject.toJSONString(list));
```

### 其它
- 略



