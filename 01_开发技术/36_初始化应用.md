## SpringBoot初始化脚本

### 概述
此针对的是SpringBoot初始化的问题，即第一次部署，自动初始化脚本和相关环境变量，
项目启动时，扫描某张表，当表记录数为0时，插入多条记录；大于0时，跳过。

### 初始化过程

执行文件中的sql语句可以是DDL脚本或DML脚本

```yaml
spring:
  datasource:
    schema: classpath:schema.sql # schema.sql中一般存放的是DDL脚本，即通常为创建或更新库表的脚本
    data: classpath:data.sql # data.sql中一般是DML脚本，即通常为数据插入脚本
```

执行多个sql文件
```yaml
pring:
  datasource:
    schema: classpath:schema_1.sql, classpath:schema_2.sql
    data: classpath:data_1.sql, classpath:data_2.sql

# 或
spring:
  datasource:
    schema:
      - classpath:schema_1.sql
      - classpath:schema_2.sql
    data:
      - classpath:data_1.sql
      - classpath:data_2.sql
```

不同环境不同脚本
```yaml
spring:
  datasource:
    schema: classpath:${spring.profiles.active:dev}/schema.sql
    data: classpath:${spring.profiles.active:dev}/data.sql
```

不同数据库

> 提醒：platform属性的默认值是'all'，所以当有在不同数据库切换的情况下才使用如上配置，因为默认值的情况下，spring boot会自动检测当前使用的数据库

```yaml
spring:
  datasource:
    schema: classpath:${spring.profiles.active:dev}/schema-${spring.datasource.platform}.sql
    data: classpath:${spring.profiles.active:dev}/data-${spring.datasource.platform}.sql
    platform: mysql
```

### 示例脚本

> 待补充

### 其它
- 略
